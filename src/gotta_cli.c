#include <string.h>
#include <stdio.h>
#include "gotta.h"
#include "gotta_net.h"

//argv[0] = Programmname
//argv[n] = Uebergabeparameter(bei programmstart)

int gotta_cli_parse_params(gotta_manager_t *gotta, int argc, char *argv[])
{
	if(argc == 2)
	{
		gotta->server.address = argv[1];
		return 0;
	}

	return 1;
}

int main(int argc, char *argv[])
{
	gotta_manager_t gotta;
	char gotta_user_name[GOTTA_USER_NAME];
	char gotta_user_password[GOTTA_USER_PASSWORD];

	memset(&gotta, 0, sizeof(gotta_manager_t));
	memset(gotta_user_name, 0, sizeof(char) * GOTTA_USER_NAME);
	memset(gotta_user_password, 0, sizeof(char) * GOTTA_USER_PASSWORD);

	if(!gotta_cli_parse_params(&gotta, argc, argv))
		gotta.server.address = GOTTA_NET_ADDRESS;

	fprintf(stdout, "Willkommen bei gotta\nBitte geben Sie Ihren Benutzernamen ein: ");
	scanf("%s", gotta_user_name);

	fprintf(stdout, "Bitte geben Sie Ihr Passwort ein: ");
	scanf("%s", gotta_user_password);

	// Implementiere gotta_net_connect();

	return 0;
}
