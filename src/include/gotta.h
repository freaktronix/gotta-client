#ifndef GOTTA_H
#define GOTTA_H

#define GOTTA_USER_NAME 16 					// Maximale Laenge Benutzername
#define GOTTA_USER_PASSWORD 32					// Maximale Laenge Passwort

typedef struct {
	struct {
		char *address;
		char *port;
	} server;
} gotta_manager_t;

#endif
