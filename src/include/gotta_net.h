#ifndef GOTTA_NET_H
#define GOTTA_NET_H

#define GOTTA_NET_ADDRESS "127.0.0.1"		// Standard IP-Adresse
#define GOTTA_NET_PORT 17420			// Standard Port

// Implementiere Uebergabeparameter
void gotta_authenticate();			// Erstellt Paket zur Uebergabe von Username und UserPW an Server
int gotta_net_connect();
int gotta_net_send();

#endif
