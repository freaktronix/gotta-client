all: gotta_cli

gotta_net.o:
	${CC} -c ./src/gotta_net.c -I ./src/include

gotta_cli: gotta_net.o
	${CC} -c ./src/gotta_cli.c -I ./src/include
	${CC} -o gotta_cli gotta_net.o gotta_cli.o

clean:
	rm -rf ./gotta_cli ./*.o
